package impls

import interfaces.Functions
import java.lang.StringBuilder
/**
 * Класс, реализующий интерфейс Functions
 */
class FunctionsImpl : Functions {
    override fun calculate(a: Int, b: Int) = a + b

    override fun substringCounter(s: String, sub: String): Int {
        val c = "\\b$sub\\b".toRegex().findAll(s).count()
        return c
    }

    override fun splitterSorter(s: String, sub: String): List<String> {
        val array = s.split(sub).toList()
        return array
    }

    override fun uniqueCounter(s: String, sub: String): Map<String, Int> {
        val resultMap = mutableMapOf<String, Int>()
        for (i in s.split(sub).iterator()) {
            if (resultMap.contains(i)) {
                resultMap.put(i, resultMap.getValue(i).inc())
            } else {
                resultMap += Pair(i, 1)
            }
        }
        return resultMap
    }

    override fun isPalindrome(s: String): Boolean {
        var result:Boolean = false
        for (i in s.length - 1 downTo 0){
            if (s[i] == s[s.length - 1 - i]){
                result = true
            }
            else{
                result = false
                break
            }
        }
        return result
    }

    override fun invert(s: String): String {
        return StringBuilder(s).reverse().toString()
    }
}
